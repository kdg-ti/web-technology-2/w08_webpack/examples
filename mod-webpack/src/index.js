import hello, {PI as pie} from "./js/hi.js";

console.log("Humble " + pie)

document.getElementById("ray").addEventListener("change", setSurface)
document.getElementById("name")
  .addEventListener("change",
    e => document.getElementById("welcome").innerText = hello(e.target.value));

function setSurface(e) {
  let ray = e.target.value
  if (!isNaN(ray))
    document.getElementById("surface").textContent = ray * ray * pie
}

// this works: with webpack the browser understands common.js
import  money from 'money-math'
console.log("Trying npm money-math package: "+ money.add("1.00","100.00"));
