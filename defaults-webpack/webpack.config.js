import path from 'path'
const config =  {
  mode:"development",
  entry: "./src/index.js",
  output: {  filename: "main.js",
    path: path.resolve("dist")
  }
}
export default config
