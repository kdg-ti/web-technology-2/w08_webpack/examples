About
=====
Webpack

Bevat `index.js`, `greeting.js`, `style.css`, `index.html` en images.
index.js staat in src.
Andere bestanden staan per type in subdirectories.
Javascript code wordt uitgesplitst in verschillende JS bronbestanden in de `js` directory.

Instructies
===========
1. `npm i`
2. `npm start`