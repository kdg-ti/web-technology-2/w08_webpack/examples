import HtmlWebpackPlugin from "html-webpack-plugin"
import path from "path";

const config = {
  devtool: "source-map",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  plugins: [new HtmlWebpackPlugin({template: "./src/html/index.html"})],
  resolve: {
    extensions: [".js",".ts"],
    extensionAlias: {
      '.js': ['.js', '.ts'],
    },
  },
  devServer: {
    static: {directory: path.resolve("dist")},
    open: true,
    hot: false
  }
}
export default config
