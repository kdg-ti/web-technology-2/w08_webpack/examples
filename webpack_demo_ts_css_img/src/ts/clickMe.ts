import puppy from "../assets/images/puppy.jpg"

export function clickMe() {
  document.getElementsByTagName("h1")
    .item(0)
    ?.addEventListener("click", () => {
      const picture= new Image();
      picture.src = puppy;
      document.querySelector("h1")?.after(picture)
    });
}
