import path from "path";
const config =  {
	devtool: 'source-map',
	mode: 'development',
	devServer: {
		static: { directory: path.resolve( "dist")},
		open: true,
		hot:false
	}
};
export default config;
