import hello, {PI as pie} from "./js/greeting.js";

console.log("Humble " + pie)

document.getElementById("ray")?.addEventListener("change", setSurface)
document.getElementById("name")?.addEventListener("change", showWelcome);
function showWelcome(e:Event) {
    const welcomeContent = document.getElementById("welcome");
    if(welcomeContent){
        welcomeContent.innerText = hello((e.target as HTMLInputElement).value);
    }
}



function setSurface(e:Event) {
    let ray = (e.target as HTMLInputElement).valueAsNumber
    if (! isNaN(ray)){
        const surfaceContent =  document.getElementById("surface");
        if(surfaceContent){
            surfaceContent.textContent = Number(ray * ray * pie).toString();
        }
    }
}

// this does not work: the browser does not understand common-js npm modules
// import  money from 'money-math'
// console.log("Trying npm money-math package: "+ money.add("1.00","100.00"));
