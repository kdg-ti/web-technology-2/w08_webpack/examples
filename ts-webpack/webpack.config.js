import path from "path";
const config =  {
	mode: 'development',
	// or devtool: 'inline-source-map'
	devtool: 'source-map',
	entry: './src/index.js',
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
		],
	},
  resolve: {
    extensions: [".ts",".js"],
    extensionAlias: {
      '.js': ['.js', '.ts'],
    },
  },
	devServer: {
		static: { directory: path.resolve( "dist")},
		open: true,
		hot:false
	}
};
export default config;
